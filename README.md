![Header](https://i.imgur.com/jo8QAaz.png)

[![Unity Version](https://img.shields.io/badge/Unity-2019.3.13f1-blue.svg)](https://unity3d.com/get-unity/download)
[![Unity Version](https://img.shields.io/badge/VisualStudio-COMMUNITY-blue.svg)](https://visualstudio.microsoft.com/downloads)
![Unity Version](https://img.shields.io/badge/supportedplatforms-ios%20%7C%20android%20%7C%20windows%20%7C%20osx-lightgrey)

A [Unity](https://unity3d.com/) frame work made for creating Space Fighter Games.
Ready to port for any platforms.

*Download the sample Android application from [G-Drive](https://drive.google.com/file/d/13uqTr_7XEHCpvkqytAoT7tbeVERiUYM_/view?usp=sharing)*

## Features

 Fight alien ships with your mother ship

 Fun fighting in the space

 Collect the power ups to boost your attack power or adding a shield

 Easy portable to iOS and Andriod


## Built With

* [Unity](http://http://unity3d.com/) - The game engine used used
* [Visual Studio Community](https://code.visualstudio.com/) - Dependency Management

## Limitations

1. Only 13 levels of enemy creations are added 
2. Images used are dummy
3. Usage of particle effects are less

## Enhancements

1. Adding more enemies by customising the level JSON
2. Adding Sounds
3. Adding support for the 3D view
4. Adding purchasable mother ships
5. Adding more purchasable weapons
6. Adding Shop Screen and Upgrade Screen
7. Adding more enemy types and mother ship types
8. Adding more power up types
9. Adding Galaxy system so that level progress can be more visualised
10. Adding shiled and power indicators (Powerup active indicators)
11. Adding proper UI
12. Adding UI transitions & particle effects
13. Adding asset bundle system

## Reference Games

[Space shooter - Galaxy attack - Galaxy shooter](https://play.google.com/store/apps/details?id=com.game.space.shooter2)

[Space Shooter - Galaxy Attack](https://play.google.com/store/apps/details?id=com.raider.spaceshooter)

[1945 Air Force](https://play.google.com/store/apps/details?id=com.os.airforce)

## Technical References

   [design-patterns](https://www.dofactory.com/net/design-patterns)
   
   [solid-design-principles](https://https://www.dotnettricks.com/learn/designpatterns/solid-design-principles-explained-using-csharp)
   
## Assets

   [Images](https://www.google.com/)
   
   [Images](https://www.photopea.com/)
   
   [Sprites](https://unitylist.com/)
   
   [JSON](http://jsoneditoronline.org/)

## Versioning

We use [Source Tree](https://www.sourcetreeapp.com/) & [Bitbucket](https://bitbucket.org) for versioning

## Authors Note

Thanks so much for checking out Space [Fighter]! Feel free to [add](mailto:sujil.v.sukumaran@gmail.com) your feedbacks!

## Authors

* **Sujil Sukumaran** - *Demo work* - [Space [Fighter]](https://bitbucket.org/sujilvs/spacefighter/src/master/)


#### *If you work at, or know an awesome studio that is looking for a developer, please reach out!*

[email](mailto:sujil.v.sukumaran@gmail.com) | [instagram](https://www.instagram.com/sujilonline/)
