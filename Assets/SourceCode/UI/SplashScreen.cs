﻿using SF.Managers;
using SF.Util;
using UIFramework;
namespace SF.UI
{
    public class SplashScreen : ScreenBase
    {
        private const float LOADING_DELAY = 3.0f;

        public override void OnShow()
        {
            base.OnShow();

            this.WaitForTime(LOADING_DELAY, () =>
            {
                GameManager.Instance.UpdateState(GameState.HomePage);
            });
        }

        public override void OnHide()
        {
            base.OnHide();
        }
    }
}
