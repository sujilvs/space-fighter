﻿using TMPro;
using SF.Managers;
using UIFramework;
using UnityEngine;
namespace SF.UI
{
    public class HomeScreen : ScreenBase
    {
        [SerializeField] private TMP_Text _lblCoinsCount;
        [SerializeField] private TMP_Text _lblXPValue;

        public override void OnShow()
        {
            base.OnShow();
            _RefreshUI();
        }

        public override void OnHide()
        {
            base.OnHide();
        }

        public override void OnButtonClick(GameObject button)
        {
            base.OnButtonClick(button);
            _OnBtnClick(button.name);
        }

        private void _OnBtnClick(string btnName)
        {
            switch (btnName)
            {
                case "Btn-Play":
                    _PlayGame();
                    break;
                case "Btn-Upgrade":
                    _OpenShop();
                    break;
            }
        }

        private void _RefreshUI()
        {
            _lblCoinsCount.text = ProfileManager.Instance.Profile.CoinCount.ToString();
            _lblXPValue.text = ProfileManager.Instance.Profile.HighScore.ToString();
        }

        private void _PlayGame()
        {
            GameManager.Instance.UpdateState(GameState.Gameplay);
        }

        private void _OpenShop()
        {
            GameManager.Instance.UpdateState(GameState.Shop);
        }
    }
}

