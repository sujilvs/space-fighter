﻿using TMPro;
using SF.Managers;
using UIFramework;
using UnityEngine;
using SF.Systems;

namespace SF.UI
{
    public class ResultScreen : ScreenBase
    {
        [SerializeField] private TMP_Text _lblCoinsCount;
        [SerializeField] private TMP_Text _totalKills;

        public override void OnShow()
        {
            base.OnShow();
            _lblCoinsCount.text = ScoreSystem.Instance.CollectedCoins.ToString();
            _totalKills.text = ScoreSystem.Instance.TotalKills.ToString();

            ProfileManager.Instance.AddCoins(ScoreSystem.Instance.CollectedCoins);
            ProfileManager.Instance.AddScore(ScoreSystem.Instance.TotalKills);
        }

        public override void OnHide()
        {
            base.OnHide();
        }

        public override void OnButtonClick(GameObject button)
        {
            base.OnButtonClick(button);
            _OnBtnClick(button.name);
        }
        private void _OnBtnClick(string btnName)
        {
            switch (btnName)
            {
                case "Btn-Home":
                    _MoveToHome();
                    break;
            }
        }

        private void _MoveToHome()
        {
            GameManager.Instance.UpdateState(GameState.HomePage);
        }
    }
}
