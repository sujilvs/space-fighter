﻿using TMPro;
using UIFramework;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using SF.Battle;
using SF.Systems;

namespace SF.UI
{
    public class GameplayScreen : ScreenBase
    {
        [SerializeField] private TMP_Text _lblCoinsCount;
        [SerializeField] private TMP_Text _lblKillCount;
        [SerializeField] List<Image> _img_lifeCount;
        [SerializeField] Sprite _lostLife;
        [SerializeField] Sprite _activeLife;

        public override void OnShow()
        {
            base.OnShow();
            _lblCoinsCount.text = 0.ToString();
            _lblKillCount.text = 0.ToString();
            _RefreshLivesImage(3);

            Messenger.Get().AddListner("CoinUpdate", _OnCoinUpdateSignal);
            Messenger.Get().AddListner("KillUpdate", _OnKillsUpdateSignal);
            Messenger.Get().AddListner("LifeUpdate", _OnLivesUpdateSignal);
        }

        public override void OnHide()
        {
            base.OnHide();
            Messenger.Get().RemoveListner("CoinUpdate", _OnCoinUpdateSignal);
            Messenger.Get().RemoveListner("KillUpdate", _OnKillsUpdateSignal);
            Messenger.Get().RemoveListner("LifeUpdate", _OnLivesUpdateSignal);
        }

        private void _OnCoinUpdateSignal(ISignal signal)
        {
            var coinUdateSignal = (CoinUpdateSignal)signal;
            _lblCoinsCount.text = coinUdateSignal.CollectedCoin.ToString();
        }
        private void _OnKillsUpdateSignal(ISignal signal)
        {
            var killUdateSignal = (KillUpdateSignal)signal;
            _lblKillCount.text = killUdateSignal.TotalKillCount.ToString();
        }

        private void _OnLivesUpdateSignal(ISignal signal)
        {
            LivesUpdateSignal livesUpdateSignal = (LivesUpdateSignal)signal;
            _RefreshLivesImage(livesUpdateSignal.RemaingLives);
        }

        private void _RefreshLivesImage(int remainingLifes)
        {
            for (int i = 0; i < _img_lifeCount.Count; i++)
            {
                if ((remainingLifes - 1) < i)
                    _img_lifeCount[i].sprite = _lostLife;
                else
                    _img_lifeCount[i].sprite = _activeLife;
            }
        }
    }
}

