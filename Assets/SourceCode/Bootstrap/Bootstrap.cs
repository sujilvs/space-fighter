﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SF.Core
{
    public class Bootstrap : SingletonBase<Bootstrap>
    {
        [SerializeField] private List<MonoBehaviour> _managers;
        private void Awake()
        {
            for (int i = 0; i < _managers.Count; i++)
            {
                var temp = (IInitialisable)_managers[i];
                temp.Init(null);
            }

            for (int i = 0; i < _managers.Count; i++)
            {
                var temp = (IInitialisable)_managers[i];
                temp.ResolveDependancies();
            }
        }
    }
}

