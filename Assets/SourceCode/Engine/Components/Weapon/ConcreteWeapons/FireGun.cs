﻿using UnityEngine;
namespace SF.Battle
{
    public class FireGun : BaseWeapon, IWeapon
    {
        [SerializeField] private GameObject _shieldObj;

        public bool HasShield { get; private set; }

        public int ScoreMultiplayer { get; set; }

        public override void Fire()
        {
            base.Fire();
        }

        public void Init(object parm)
        {

        }

        public override void Remove()
        {
            base.Remove();
        }

        public void ResolveDependancies()
        {
            throw new System.NotImplementedException();
        }

        public override void RestAfterPU()
        {
            _shieldObj.SetActive(false);
            HasShield = false;
            base.RestAfterPU();
        }

        public void UpdateWeaponPower(int fire)
        {
            _weaponPower = fire;
        }

        public void UpdateWeaponPower(bool shield)
        {
            HasShield = shield;
            _shieldObj.SetActive(true);
        }

        public void Upgrade(IPowerUp powerUp)
        {
            throw new System.NotImplementedException();
        }

        public void UpgradePower(IPowerUp powerUp)
        {
            throw new System.NotImplementedException();
        }
    }
}
