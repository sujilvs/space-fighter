﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SF.Battle
{
    public abstract class BaseWeapon : MonoBehaviour
    {
        protected GameObject _bulletPrefab;
        protected ShipBody _shipBody;
        protected float _fireRate;
        protected float _nextFire;
        protected int _weaponPower = 1;

        private GameObject _PURT_bulletPrefab;
        private int _PURT_weaponPower;

        private void MakeAShot()
        {
            switch (_weaponPower)
            {
                case 1:
                    CreateLazerShot(_bulletPrefab, _shipBody._mainGunPoint.transform.position, Vector3.zero);
                    _shipBody.MainGunVFX.Play();
                    break;
                case 2:
                    CreateLazerShot(_bulletPrefab, _shipBody._rightGunPoint.transform.position, Vector3.zero);
                    _shipBody.LeftGunVFX.Play();
                    CreateLazerShot(_bulletPrefab, _shipBody._leftGunPoint.transform.position, Vector3.zero);
                    _shipBody.RightGunVFX.Play();
                    break;
                case 3:
                    CreateLazerShot(_bulletPrefab, _shipBody._mainGunPoint.transform.position, Vector3.zero);
                    CreateLazerShot(_bulletPrefab, _shipBody._rightGunPoint.transform.position, new Vector3(0, 0, -5));
                    _shipBody.LeftGunVFX.Play();
                    CreateLazerShot(_bulletPrefab, _shipBody._leftGunPoint.transform.position, new Vector3(0, 0, 5));
                    _shipBody.RightGunVFX.Play();
                    break;
                case 4:
                    CreateLazerShot(_bulletPrefab, _shipBody._mainGunPoint.transform.position, Vector3.zero);
                    CreateLazerShot(_bulletPrefab, _shipBody._rightGunPoint.transform.position, new Vector3(0, 0, -5));
                    _shipBody.LeftGunVFX.Play();
                    CreateLazerShot(_bulletPrefab, _shipBody._leftGunPoint.transform.position, new Vector3(0, 0, 5));
                    _shipBody.RightGunVFX.Play();
                    CreateLazerShot(_bulletPrefab, _shipBody._leftGunPoint.transform.position, new Vector3(0, 0, 15));
                    CreateLazerShot(_bulletPrefab, _shipBody._rightGunPoint.transform.position, new Vector3(0, 0, -15));
                    break;
            }
        }

        public virtual void Fire()
        {
            if (Time.time > _nextFire)
            {
                MakeAShot();
                _nextFire = Time.time + 1 / _fireRate;
            }
        }

        public virtual void Construct(ShipBody shipBody, GameObject bullet, float fireRate, int weaponPower)
        {
            this._PURT_bulletPrefab = bullet;
            this._PURT_weaponPower = weaponPower;

            this._shipBody = shipBody;
            this._bulletPrefab = bullet;
            this._fireRate = fireRate;
            this._weaponPower = weaponPower;
        }

        public virtual void RestAfterPU()
        {
            Debug.Log("Current Power " + _PURT_weaponPower);

            this._weaponPower = _PURT_weaponPower;
            //_bulletPrefab = _PURT_bulletPrefab;
        }

        public virtual void Remove()
        {
            Destroy(this.gameObject);
        }

        private void CreateLazerShot(GameObject lazer, Vector3 pos, Vector3 rot) //translating 'pooled' lazer shot to the defined position in the defined rotation
        {
            GameObject amo = Instantiate(lazer, pos, Quaternion.Euler(rot));
        }
    }
}
