﻿using UnityEngine;
namespace SF.Battle
{
    public class SonicBeam : BaseAmo
    {
        protected override void DealTheCollision(Collider2D collision)
        {
            if (collision.tag == "Enemy")
            {
                collision.GetComponent<EnemyShip>().TakeDamage(Damage);
                Destruction();
            }
        }
    }
}
