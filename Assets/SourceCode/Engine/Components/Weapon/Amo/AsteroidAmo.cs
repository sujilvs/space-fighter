﻿using UnityEngine;
namespace SF.Battle
{
    public class AsteroidAmo : BaseAmo
    {
        protected override void DealTheCollision(Collider2D collision)
        {
            if (collision.tag == "Player")
            {
                collision.GetComponent<PlayerShip>().TakeDamage(Damage);
                Destruction();
            }
        }
    }
}

