﻿using UnityEngine;
namespace SF.Battle
{
    public abstract class BaseAmo : MonoBehaviour, IAmo
    {
        [SerializeField] private int _damage;

        public int Damage { get { return _damage; } }

        protected abstract void DealTheCollision(Collider2D collision);

        private void OnTriggerEnter2D(Collider2D collision)
        {
            DealTheCollision(collision);
        }

        public void Destruction()
        {
            Destroy(gameObject);
        }
    }
}

