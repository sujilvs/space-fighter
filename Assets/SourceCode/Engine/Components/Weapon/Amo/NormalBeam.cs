﻿using UnityEngine;
namespace SF.Battle
{
    public class NormalBeam : BaseAmo
    {
        protected override void DealTheCollision(Collider2D collision)
        {
            if (collision.tag == "Enemy")
            {
                collision.GetComponent<EnemyShuttle>().TakesDamage(Damage);
                Destruction();
            }
        }
    }
}

