﻿using UnityEngine;
namespace SF.Battle
{
    public class WeaponBuilder : SingletonBase<WeaponBuilder>
    {
        [SerializeField] private GameObject _bullet;
        [SerializeField] private FireGun _fireGunPrefab;

        public IWeapon CreateDefaultWeapon(ShipBody shipBody)
        {
            var weaponGO = Instantiate(_fireGunPrefab);
            IWeapon weapon = weaponGO.GetComponent<IWeapon>();
            weapon.Construct(shipBody, _bullet, 3.5f, 1);
            return weapon;
        }
    }
}

