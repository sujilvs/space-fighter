﻿using UnityEngine;
namespace SF.Battle
{
    public interface IWeapon
    {
        void Construct(ShipBody shipBody, GameObject bullet, float fireRate, int weaponPower);
        void Fire();
        void Upgrade(IPowerUp powerUp);
        void UpdateWeaponPower(int weaponPower);
        void UpdateWeaponPower(bool shield);
        void RestAfterPU();
        bool HasShield { get; }
        int ScoreMultiplayer { get; set; }
        void Remove();
    }
}
