﻿using UnityEngine;
namespace SF.Battle
{
    public class ShipBody : MonoBehaviour
    {
        public GameObject _mainGunPoint;
        public GameObject _leftGunPoint;
        public GameObject _rightGunPoint;
        public GameObject _sonicGunPoint;

        public ParticleSystem MainGunVFX { get; private set; }
        public ParticleSystem LeftGunVFX { get; private set; }
        public ParticleSystem RightGunVFX { get; private set; }
        public ParticleSystem SonicGunVFX { get; private set; }

        private void Start()
        {
            MainGunVFX = _mainGunPoint.GetComponent<ParticleSystem>();
            LeftGunVFX = _leftGunPoint.GetComponent<ParticleSystem>();
            RightGunVFX = _rightGunPoint.GetComponent<ParticleSystem>();
            SonicGunVFX = _sonicGunPoint.GetComponent<ParticleSystem>();
        }
    }
}

