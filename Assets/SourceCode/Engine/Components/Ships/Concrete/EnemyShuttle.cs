﻿using SF.Managers;
using SF.Systems;
using UnityEngine;
namespace SF.Battle
{
    public class EnemyShuttle : MonoBehaviour
    {
        [SerializeField] private GameObject Projectile;
        [SerializeField] private GameObject destructionVFX;
        [SerializeField] private GameObject hitEffect;

        private int _shotChance;
        private int _health;

        public void ActivateShuttle(int health, int shotChance, float shotTimeMin, float shotTimeMax)
        {
            GarbageCollector.Instance.Add(this.gameObject);
            _health = health;
            _shotChance = shotChance;
            Invoke("ActivateShooting", Random.Range(shotTimeMin, shotTimeMax));
        }

        private void ActivateShooting()
        {
            if (Random.value < (float)_shotChance / 100)
            {
                Instantiate(Projectile, gameObject.transform.position, Quaternion.identity);
                //TODO - Add Object Pooling
            }
        }

        public void TakesDamage(int damage)
        {
            _health -= damage;
            if (_health <= 0)
                Destruction();
            else
                Instantiate(hitEffect, transform.position, Quaternion.identity, transform);
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.tag == "Player")
            {
                collision.GetComponent<IShip>().TakeDamage(1);
                _ShowEffectAndRemove();
            }
        }

        private void Destruction()
        {
            ScoreSystem.Instance.AddKills(1);
            ScoreSystem.Instance.AddCoins(3);
            _ShowEffectAndRemove();
        }

        private void _ShowEffectAndRemove()
        {
            Instantiate(destructionVFX, transform.position, Quaternion.identity);
            GarbageCollector.Instance.Remove(this.gameObject);
            Destroy(gameObject);
        }
    }
}
