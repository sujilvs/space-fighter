﻿using System;
using System.Collections;
using SF.Core;
using SF.Managers;
using UnityEngine;
namespace SF.Battle
{
    public class PlayerShip : BaseShip, IShip, IInitialisable, IInputListner
    {
        [SerializeField] private ShipBody _shipBody;
        [SerializeField] private GameObject _destructionFX;
        [SerializeField] private BoxCollider2D _boxCollider;

        private Borders _borders;
        private GameState _playableGameState = GameState.None;
        private IWeapon _weapon;
        private int _totallifeCount;

        public IWeapon Weapon { get { return _weapon; } }

        public void Init(object parm)
        {
            _playableGameState = GameState.None;

        }

        public void BeginFight()
        {
            _playableGameState = GameState.Gameplay;
            _totallifeCount = 3;

            //_boxCollider.enabled = false;

            //MoveToPosition(this.gameObject, new Vector3(0f, 8f, 0f), 2.0f, () =>
            //{
            //    this.transform.localPosition = new Vector3(0f, -7f, 0f);
            //    MoveToPosition(this.gameObject, new Vector3(0f, -3f, 0f), 2.0f, () =>
            //    {
            //        _boxCollider.enabled = true;
            //        Debug.Log("Movement is done");
            //    });
            //    Debug.Log("Movement is done");
            //});

            this.transform.localPosition = new Vector3(0f, -3f, 0f);

            Messenger.Get().BroadCast(new LivesUpdateSignal() { SignalType = "LifeUpdate", RemaingLives = _totallifeCount });

            this.gameObject.SetActive(true);
            InputManager.Instance.Register(this);
            InputManager.Instance.OnInputUpdate += OnPointerUpdate;

            _weapon = WeaponBuilder.Instance.CreateDefaultWeapon(_shipBody);
            BaseWeapon defaulWpn = (BaseWeapon)_weapon;

            defaulWpn.transform.SetParent(this.transform);
            defaulWpn.transform.localPosition = Vector3.zero;
        }

        public void PrepareForBattle()
        {
            this.gameObject.SetActive(true);

            this.transform.localPosition = new Vector3(0f, -7f, 0f);

            MoveToPosition(this.gameObject, new Vector3(0f, 0.57f, 0f), 5.0f, ()=>
            {
                Debug.Log("Movement is done");
            });
        }

        public void EndFight()
        {
            _playableGameState = GameState.None;
            InputManager.Instance.UnRegister(this);
            InputManager.Instance.OnInputUpdate -= OnPointerUpdate;

            if (_weapon != null)
            {
                _weapon.Remove();
                _weapon = null;
            }
        }

        public void Fire()
        {
            _weapon.Fire();
        }

        public void TakeDamage(int damage)
        {
            if (_weapon == null)
                return;

            if (_weapon.HasShield)
                return;

            _totallifeCount -= damage;

            Messenger.Get().BroadCast(new LivesUpdateSignal() { SignalType = "LifeUpdate", RemaingLives = Mathf.Clamp(_totallifeCount, 0, 3) });

            if (_totallifeCount <= 0)
            {
                _Destruction();
            }
        }

        private void _Destruction()
        {
            GameManager.Instance.UpdateState(GameState.Result);
            //TODO - Add Object Pool
            Instantiate(_destructionFX, transform.position, Quaternion.identity);
            this.gameObject.SetActive(false);
        }

        public void OnPointerUpdate(Vector3 position)
        {
            if (_playableGameState == GameState.Gameplay)
            {
                Move(position);
                Fire();
            }
        }

        public void UpdateBorders(Borders borders)
        {
            _borders = borders;
        }

        public void Move()
        {
            throw new System.NotImplementedException();
        }

        public void Move(Vector3 position)
        {
            position.z = transform.position.z;
            transform.position = Vector3.MoveTowards(transform.position, position, 30 * Time.deltaTime);
            transform.position = new Vector3(Mathf.Clamp(transform.position.x, _borders.minX, _borders.maxX), Mathf.Clamp(transform.position.y, _borders.minY, _borders.maxY), 0);
        }

        public void ResolveDependancies()
        {

        }

        public void Terminate()
        {
            throw new System.NotImplementedException();
        }

        private void MoveToPosition(GameObject obj, Vector3 destination, float speed, Action OnMoveDone = null)
        {
            StartCoroutine(_WaitForMove(obj, destination, speed, OnMoveDone));
        }

        private IEnumerator _WaitForMove(GameObject obj, Vector3 destination, float speed, Action OnMoveDone = null)
        {

            //Debug.Log(Vector3.Distance(obj.transform.localPosition, destination));
            while (Vector3.Distance(obj.transform.localPosition, destination) > 0.1f)
            {
                obj.transform.localPosition = Vector3.Lerp (obj.transform.localPosition, destination, speed * Time.deltaTime);
                yield return null;
            }
            OnMoveDone?.Invoke();
        }
    }

    public sealed class LivesUpdateSignal : ISignal
    {
        public string SignalType { get; set; }
        public int RemaingLives { get; set; }
    }
}
