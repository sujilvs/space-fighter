﻿using System;
using UnityEngine;
using System.Collections;
using SF.Core;
using SF.Systems;
using SF.Util;

namespace SF.Battle
{
    public class EnemyShip : BaseShip, IShip, IInitialisable
    {
        public GameObject enemy;
        public Transform[] pathPoints;

        private int count;
        private float speed;
        private float timeBetween;
        private bool rotationByPath;
        private bool Loop;
        private Color pathColor = Color.yellow;
        private int life;

        private int ShotChance;
        private float ShotTimeMin, ShotTimeMax;

        public bool testMode;

        public IWeapon Weapon => throw new NotImplementedException();

        public void Fire()
        {
            throw new System.NotImplementedException();
        }

        public void Init(object parm)
        {
            WaveData waveData = (WaveData)parm;
            _ApplyData(waveData.enemyShipData);
            StartCoroutine(CreateEnemyWave());
        }

        public void Move()
        {
            throw new System.NotImplementedException();
        }

        public void Move(Vector3 position)
        {
            throw new System.NotImplementedException();
        }

        public void ResolveDependancies()
        {
            throw new System.NotImplementedException();
        }

        public void TakeDamage(int percentage)
        {
            throw new System.NotImplementedException();
        }

        private IEnumerator CreateEnemyWave()
        {
            for (int i = 0; i < count; i++)
            {
                //TODO Add Object Pooling
                GameObject newEnemy = Instantiate(enemy, enemy.transform.position, Quaternion.identity); ;

                FollowThePath followComponent = newEnemy.GetComponent<FollowThePath>();
                followComponent.path = pathPoints;
                followComponent.speed = speed;
                followComponent.rotationByPath = rotationByPath;
                followComponent.loop = Loop;
                followComponent.SetPath();

                EnemyShuttle enemyComponent = newEnemy.GetComponent<EnemyShuttle>();
                enemyComponent.ActivateShuttle(life, ShotChance, ShotTimeMin, ShotTimeMax);
                newEnemy.SetActive(true);
                yield return new WaitForSeconds(timeBetween);
            }
            if (testMode)
            {
                yield return new WaitForSeconds(3);
                StartCoroutine(CreateEnemyWave());
            }
            else if (!Loop)
                Destroy(gameObject);
        }

        private void _ApplyData(EnemyShipData enemyShipData)
        {
            for (int i = 0; i < enemyShipData.positions.Count; i++)
            {
                var pos = enemyShipData.positions[i];
                pos.y -= 2f;
                enemyShipData.positions[i] = pos;
            }

            for (int i = 0; i < pathPoints.Length; i++)
            {
                pathPoints[i].localPosition = enemyShipData.positions[i];
            }


            count = enemyShipData.count;
            speed = enemyShipData.speed;
            timeBetween = enemyShipData.timeBetween;
            rotationByPath = enemyShipData.rotationByPath;
            Loop = enemyShipData.loop;
            life = 3;


            ShotChance = enemyShipData.shotChance;
            ShotTimeMin = enemyShipData.shotTimeMin;
            ShotTimeMax = enemyShipData.shotTimeMax;

            _CreatePath(pathPoints);
        }

        private void _CreatePath(Transform[] path)
        {
            Vector3[] pathPositions = new Vector3[path.Length];
            for (int i = 0; i < path.Length; i++)
            {
                pathPositions[i] = path[i].position;
            }
            Vector3[] newPathPositions = _CreatePoints(pathPositions);
            Vector3 previosPositions = _Interpolate(newPathPositions, 0);
            Gizmos.color = pathColor;
            int SmoothAmount = path.Length * 20;
            for (int i = 1; i <= SmoothAmount; i++)
            {
                float t = (float)i / SmoothAmount;
                Vector3 currentPositions = _Interpolate(newPathPositions, t);
                previosPositions = currentPositions;
            }
        }

        private Vector3 _Interpolate(Vector3[] path, float t)
        {
            int numSections = path.Length - 3;
            int currPt = Mathf.Min(Mathf.FloorToInt(t * numSections), numSections - 1);
            float u = t * numSections - currPt;
            Vector3 a = path[currPt];
            Vector3 b = path[currPt + 1];
            Vector3 c = path[currPt + 2];
            Vector3 d = path[currPt + 3];
            return 0.5f * ((-a + 3f * b - 3f * c + d) * (u * u * u) + (2f * a - 5f * b + 4f * c - d) * (u * u) + (-a + c) * u + 2f * b);
        }

        private Vector3[] _CreatePoints(Vector3[] path)
        {
            Vector3[] pathPositions;
            Vector3[] newPathPos;
            int dist = 2;
            pathPositions = path;
            newPathPos = new Vector3[pathPositions.Length + dist];
            Array.Copy(pathPositions, 0, newPathPos, 1, pathPositions.Length);
            newPathPos[0] = newPathPos[1] + (newPathPos[1] - newPathPos[2]);
            newPathPos[newPathPos.Length - 1] = newPathPos[newPathPos.Length - 2] + (newPathPos[newPathPos.Length - 2] - newPathPos[newPathPos.Length - 3]);
            if (newPathPos[1] == newPathPos[newPathPos.Length - 2])
            {
                Vector3[] LoopSpline = new Vector3[newPathPos.Length];
                Array.Copy(newPathPos, LoopSpline, newPathPos.Length);
                LoopSpline[0] = LoopSpline[LoopSpline.Length - 3];
                LoopSpline[LoopSpline.Length - 1] = LoopSpline[2];
                newPathPos = new Vector3[LoopSpline.Length];
                Array.Copy(LoopSpline, newPathPos, LoopSpline.Length);
            }
            return (newPathPos);
        }

        public void Terminate()
        {
            Debug.LogError("Terminate Call came");
            Destroy(this);
        }
    }
}