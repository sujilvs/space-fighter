﻿using UnityEngine;
namespace SF.Battle
{
    public interface IShip
    {
        void Fire();
        void TakeDamage(int damage);
        void Move();
        void Move(Vector3 position);
        IWeapon Weapon { get; }
    }
}

