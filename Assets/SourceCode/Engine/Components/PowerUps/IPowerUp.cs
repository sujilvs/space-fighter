﻿using System;
namespace SF.Battle
{
    public interface IPowerUp
    {
        IPowerUp Create();
        void Activate(IWeapon weapon, Action OnPUDone);
    }
}
