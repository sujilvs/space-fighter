﻿using SF.Util;
using UnityEngine;
namespace SF.Battle
{
    public class PowerAttack : BasePowerUp, IPowerUp
    {
        private int _attackValue;
        private float _timeOut;

        public override IPowerUp Create()
        {
            _attackValue = Random.Range(2, 5);
            _timeOut = Random.Range(5.0f, 10.0f);
            var powerAttack = this.GetComponent<PowerAttack>();
            return powerAttack;
        }

        protected override void ActivatePU(IWeapon weapon)
        {
            weapon.UpdateWeaponPower(_attackValue);
            this.WaitForTime(_timeOut, () =>
            {
                Debug.Log("Reste The PU");
                weapon.RestAfterPU();
                Destroy(this);
            });
        }
    }
}
