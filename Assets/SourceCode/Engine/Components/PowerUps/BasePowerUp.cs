﻿using System;
using UnityEngine;
namespace SF.Battle
{
    public abstract class BasePowerUp : MonoBehaviour
    {
        [SerializeField] protected GameObject _vfx;
        public Renderer Renderer { get { return _vfx.GetComponent<Renderer>(); } }

        private bool _isActivated = false;

        public virtual IPowerUp Create()
        {
            return null;
        }
        public virtual void Activate(IWeapon weapon, Action OnPUDone)
        {

        }
        protected abstract void ActivatePU(IWeapon weapon);

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.tag == "Player")
            {
                ActivatePU(collision.GetComponent<IShip>().Weapon);
                _isActivated = true;
                _HideUntilPowerDeativate();
            }
        }

        protected virtual void _HideUntilPowerDeativate()
        {
            _vfx.SetActive(false);
        }

        public void UpdateOnBorderCollision()
        {
            if (!_isActivated)
                Destroy(this);
        }
    }
}
