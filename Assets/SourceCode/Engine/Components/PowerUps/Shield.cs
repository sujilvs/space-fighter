﻿using SF.Util;
using UnityEngine;
namespace SF.Battle
{
    public class Shield : BasePowerUp, IPowerUp
    {
        private float _timeOut;

        public override IPowerUp Create()
        {
            _timeOut = Random.Range(5.0f, 10.0f);
            var shield = this.GetComponent<Shield>();
            return shield;
        }

        protected override void ActivatePU(IWeapon weapon)
        {
            if (weapon == null)
                return;

            weapon.UpdateWeaponPower(true);
            this.WaitForTime(_timeOut, () =>
            {
                Debug.Log("Reste The PU");
                weapon.RestAfterPU();
                Destroy(this);
            });
        }
    }
}

