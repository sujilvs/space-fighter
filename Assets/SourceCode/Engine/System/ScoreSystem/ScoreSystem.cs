﻿using SF.Core;
namespace SF.Systems
{
    public class ScoreSystem : SingletonBase<ScoreSystem>, IGameStateListener, IInitialisable
    {
        private int _totalKills;
        private int _collectedCoins;

        public int TotalKills { get { return _totalKills; } }
        public int CollectedCoins { get { return _collectedCoins; } }

        public void Init(object parm)
        {
            //throw new System.NotImplementedException();
        }

        public void OnStateChange(GameState prevState, GameState curState, object parms = null)
        {
            switch (curState)
            {
                case GameState.Gameplay:
                    break;
                case GameState.Result:
                    break;
            }
        }

        public void AddCoins(int coinCount)
        {
            _collectedCoins += coinCount;
            Messenger.Get().BroadCast(new CoinUpdateSignal() { SignalType = "CoinUpdate", CollectedCoin = _collectedCoins });
        }

        public void AddKills(int killCount)
        {
            _totalKills += killCount;
            Messenger.Get().BroadCast(new KillUpdateSignal() { SignalType = "KillUpdate", TotalKillCount = _totalKills });
        }

        public void ResolveDependancies()
        {
            throw new System.NotImplementedException();
        }

        private void _StartScoreSystem()
        {
            _totalKills = 0;
            _collectedCoins = 0;
        }

        private void _AddToProfile()
        {

        }

        public void Terminate()
        {
            throw new System.NotImplementedException();
        }
    }

    public sealed class KillUpdateSignal : ISignal
    {
        public string SignalType { get; set; }
        public int TotalKillCount { get; set; }
    }

    public sealed class CoinUpdateSignal : ISignal
    {
        public string SignalType { get; set; }
        public int CollectedCoin { get; set; }
    }
}
