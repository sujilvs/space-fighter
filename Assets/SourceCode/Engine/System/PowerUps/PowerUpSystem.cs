﻿using System.Collections;
using System.Collections.Generic;
using SF.Battle;
using SF.Core;
using SF.Managers;
using UnityEngine;
namespace SF.Systems
{
    public class PowerUpSystem : SingletonBase<PowerUpSystem>, IGameStateListener, IInitialisable, IInputListner
    {
        [SerializeField] private float _powerUpInterval;
        [SerializeField] private List<BasePowerUp> _powerUp;

        private Camera _camera;
        private Borders _borders;
        private GameState _activeState;


        public void Init(object parm)
        {
            _camera = Camera.main;
            InputManager.Instance.Register(this);
            GameManager.Instance.SubscribeForStateChange(this);
        }

        public void OnStateChange(GameState prevState, GameState curState, object parms = null)
        {
            _activeState = curState;

            switch (curState)
            {
                case GameState.Gameplay:
                    StartCoroutine(PowerupBonusCreation());
                    break;
                default:
                    StopAllCoroutines();
                    break;
            }
        }

        private IEnumerator PowerupBonusCreation()
        {
            while (true)
            {
                yield return new WaitForSeconds(_powerUpInterval);
                if (_activeState == GameState.Gameplay)
                {
                    int randIndex = Random.Range(0, _powerUp.Count);
                    var some = Instantiate(_powerUp[randIndex], new Vector2(Random.Range(_borders.minX, _borders.maxX),
                         _camera.ViewportToWorldPoint(Vector2.up).y + _powerUp[randIndex].GetComponent<BasePowerUp>().Renderer.bounds.size.y / 2), Quaternion.identity);
                    some.GetComponent<IPowerUp>().Create();
                    //TODO - RESET PLACE

                }
            }
        }

        public void ResolveDependancies()
        {
            //throw new System.NotImplementedException();
        }

        public void UpdateBorders(Borders borders)
        {
            _borders = borders;
            //throw new System.NotImplementedException();
        }

        public void OnPointerUpdate(Vector3 position)
        {
            //throw new System.NotImplementedException();
        }

        public void Terminate()
        {
            throw new System.NotImplementedException();
        }
    }

}
