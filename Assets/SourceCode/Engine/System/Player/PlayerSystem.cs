﻿using SF.Battle;
using SF.Core;
using SF.Managers;
using UnityEngine;
namespace SF.Systems
{
    public class PlayerSystem : SingletonBase<PlayerSystem>, IGameStateListener, IInitialisable
    {
        [SerializeField] private PlayerShip PlayerShip;

        public void Init(object parm)
        {
            GameManager.Instance.SubscribeForStateChange(this);
            PlayerShip.Init(null);
        }

        public void OnStateChange(GameState prevState, GameState curState, object parms = null)
        {
            switch (curState)
            {
                case GameState.Gameplay:
                    PlayerShip.BeginFight();
                    break;
                case GameState.HomePage:
                    PlayerShip.PrepareForBattle();
                    break;
                default:
                    PlayerShip.EndFight();
                    break;
            }
        }

        public void ResolveDependancies()
        {

        }

        public void Terminate()
        {
            throw new System.NotImplementedException();
        }
    }
}
