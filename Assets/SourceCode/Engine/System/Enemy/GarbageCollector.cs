﻿using System.Collections;
using System.Collections.Generic;
using SF.Battle;
using UnityEngine;

public class GarbageCollector : SingletonBase<GarbageCollector>
{
    private List<GameObject> _trackingObjects;

    public void Add(GameObject  generatedObject)
    {
        Init();
        //Debug.Log("Added");
        _trackingObjects.Add(generatedObject);
    }

    public void Remove(GameObject generatedObject)
    {
        Init();
        //Debug.Log("Removed");

        _trackingObjects.Remove(generatedObject);
    }

    public void TerminateAll ()
    {
        if (_trackingObjects != null)
        {
            //Debug.Log("Terminating " + _trackingObjects.Count);
            for (int i = 0; i < _trackingObjects.Count; i++)
            {
                Destroy(_trackingObjects[i]);
            }
            _trackingObjects.Clear();
        }
    }

    private void Init()
    {
        if(_trackingObjects == null)
        {
            _trackingObjects = new List<GameObject>();
        }
   
    }
}
