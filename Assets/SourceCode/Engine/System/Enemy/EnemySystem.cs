﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using SF.Managers;
using SF.Battle;
using SF.Core;

namespace SF.Systems
{
    public class EnemySystem : SingletonBase<EnemySystem>, IGameStateListener, IInitialisable
    {
        [SerializeField] private TextAsset _enemyWaveData;
        [SerializeField] private GameObject _enemyShipPrefab;

        private GameState _curState;

        private List<WaveData> _enemyWaveDataObj;

        public void Init(object parm)
        {
            GameManager.Instance.SubscribeForStateChange(this);
        }

        private IEnumerator CreateEnemyWave(float delay, GameObject Wave, WaveData waveData)
        {
            if (delay != 0)
                yield return new WaitForSeconds(delay);

            if (_curState == GameState.Gameplay)
            {
                GameObject generated = Instantiate(Wave);
                Debug.Log(generated.name);
                var enemyShip = generated.GetComponent<EnemyShip>();
                enemyShip.Init(waveData);
                _RepeatOnceAllEnemiesGenerated(delay);
            }
        }

        private void _RepeatOnceAllEnemiesGenerated(float delay)
        {
            if (delay == _enemyWaveDataObj[_enemyWaveDataObj.Count - 1].timeToStart)
            {
                Debug.Log("Generating another set");
                _StartEnemySpwan();
            }
        }

        public void OnStateChange(GameState prevState, GameState curState, object parms = null)
        {
            _curState = curState;
            switch (curState)
            {
                case GameState.Gameplay:
                    string loadedJSON = _enemyWaveData.text;
                    _enemyWaveDataObj = new List<WaveData>();
                    _enemyWaveDataObj = JsonConvert.DeserializeObject<List<WaveData>>(loadedJSON);
                    _StartEnemySpwan();
                    break;
                case GameState.HomePage:
                    if (prevState == GameState.Result)
                    {
                        StopAllCoroutines();
                        Terminate();
                    }
                    break;
            }
        }

        public void ResolveDependancies()
        {
        }

        private void _StartEnemySpwan()
        {
            for (int i = 0; i < _enemyWaveDataObj.Count; i++)
            {
                StartCoroutine(CreateEnemyWave(_enemyWaveDataObj[i].timeToStart, _enemyShipPrefab, _enemyWaveDataObj[i]));
            }
        }

        public void Terminate()
        {
            GarbageCollector.Instance.TerminateAll();
        }
    }

    [System.Serializable]
    public class WaveData
    {
        public float timeToStart;
        public EnemyShipData enemyShipData;
    }

    [System.Serializable]
    public class EnemyShipData
    {
        public int count;
        public float speed;
        public float timeBetween;
        public List<Vector3> positions;
        public bool rotationByPath;
        public bool loop;
        public int shotChance;
        public float shotTimeMin, shotTimeMax;
    }
}
