﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public interface IGameStateListener
{
    void OnStateChange(GameState prevState, GameState curState, object parms = null);
}
