﻿using UnityEngine;
using Newtonsoft.Json;
using SF.Core;

namespace SF.Managers
{
    public class ProfileManager : SingletonBase<ProfileManager>, IInitialisable
    {
        private const string PLAYER_DATA = "player_data";
        private const int DEFAULT_COIN_COUNT = 10;
        private const int DEFAULT_HIGH_SCORE = 0;

        public Profile Profile { get; private set; }

        public void Init(object parm)
        {
            if (PlayerPrefs.HasKey(PLAYER_DATA))
            {
                string loadedJSON = PlayerPrefs.GetString(PLAYER_DATA);
                Profile = JsonConvert.DeserializeObject<Profile>(loadedJSON);
                SaveData();
            }
            else
            {
                Profile = new Profile() { CoinCount = DEFAULT_COIN_COUNT, HighScore = DEFAULT_HIGH_SCORE };
            }
        }

        public void ResolveDependancies()
        {

        }

        public void AddCoins(int coins)
        {
            Profile.CoinCount += coins;
            SaveData();
        }

        public void AddScore(int score)
        {
            if (Profile.HighScore < score)
                Profile.HighScore = score;
            SaveData();
        }

        private void SaveData()
        {
            var jsonData = JsonConvert.SerializeObject(Profile);
            PlayerPrefs.SetString(PLAYER_DATA, jsonData);
        }

        public void Terminate()
        {
            throw new System.NotImplementedException();
        }
    }

    public class Profile
    {
        public int CoinCount;
        public int HighScore;
        //TODO - Owner weapon details can be added later
    }
}
