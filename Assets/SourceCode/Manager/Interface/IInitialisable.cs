﻿namespace SF.Core
{
    public interface IInitialisable
    {
        void Init(object parm);
        void ResolveDependancies();
        void Terminate();
    }
}
