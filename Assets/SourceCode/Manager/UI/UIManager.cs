﻿using SF.Core;
using SF.Managers;
namespace SF.UI
{
    public class UIManager : SingletonBase<UIManager>, IInitialisable, IGameStateListener
    {
        public void Init(object parm)
        {
            GameManager.Instance.SubscribeForStateChange(this);
        }

        public void OnStateChange(GameState prevState, GameState curState, object parms = null)
        {
            switch (prevState)
            {
                case GameState.None:
                    break;
                case GameState.Splash:
                    ScreenFactory.Instance.Deactivate<SplashScreen>();
                    break;
                case GameState.HomePage:
                    ScreenFactory.Instance.Deactivate<HomeScreen>();
                    break;
                case GameState.Gameplay:
                    ScreenFactory.Instance.Deactivate<GameplayScreen>();
                    break;
                case GameState.Result:
                    ScreenFactory.Instance.Deactivate<ResultScreen>();
                    break;
                case GameState.Shop:
                    break;
            }

            switch (curState)
            {
                case GameState.Splash:
                    ScreenFactory.Instance.Activate<SplashScreen>();
                    break;
                case GameState.HomePage:
                    ScreenFactory.Instance.Activate<HomeScreen>();
                    break;
                case GameState.Gameplay:
                    ScreenFactory.Instance.Activate<GameplayScreen>();
                    break;
                case GameState.Result:
                    ScreenFactory.Instance.Activate<ResultScreen>();
                    break;
                case GameState.Shop:
                    break;
            }
        }

        public void ResolveDependancies()
        {
        }

        public void Terminate()
        {
            throw new System.NotImplementedException();
        }
    }
}
