﻿using SF.Managers;
using UnityEngine;
namespace SF.Core
{
    public interface IInputListner
    {
        void UpdateBorders(Borders borders);
        void OnPointerUpdate(Vector3 position);
        //void UpdateBorders(Borders borders);
    }
}
