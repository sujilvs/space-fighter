﻿using SF.Core;
using UnityEngine;
using System.Collections.Generic;
namespace SF.Managers
{
    public class InputManager : SingletonBase<InputManager>, IInitialisable
    {
        public Borders borders;

        private List<IInputListner> _activeListners;
        private Camera _activeCamera;
        public delegate void InputUpdate(Vector3 position);
        public InputUpdate OnInputUpdate;


        public void Init(object parm)
        {
            _activeListners = new List<IInputListner>();
            _activeCamera = Camera.main;
            _ResizeBorders();
        }

        public void Register(IInputListner inputListner)
        {
            if (!_activeListners.Contains(inputListner))
            {
                _activeListners.Add(inputListner);
                inputListner.UpdateBorders(borders);
            }
        }
        public void UnRegister(IInputListner inputListner)
        {
            if (_activeListners.Contains(inputListner))
            {
                _activeListners.Remove(inputListner);
            }
        }

        private void _BroadCastInputUpdate(Vector3 position)
        {
            OnInputUpdate?.Invoke(position);
        }


        private void Update()
        {
#if UNITY_STANDALONE || UNITY_EDITOR
            if (Input.GetMouseButton(0))
            {
                Vector3 mousePosition = _activeCamera.ScreenToWorldPoint(Input.mousePosition);
                _BroadCastInputUpdate(mousePosition);
            }
#endif

#if UNITY_IOS || UNITY_ANDROID

            if (Input.touchCount == 1)
            {
                Touch touch = Input.touches[0];
                Vector3 touchPosition = _activeCamera.ScreenToWorldPoint(touch.position);
                _BroadCastInputUpdate(touchPosition);
            }
#endif
        }

        public void ResolveDependancies()
        {

        }

        private void _ResizeBorders()
        {
            borders.minX = _activeCamera.ViewportToWorldPoint(Vector2.zero).x + borders.minXOffset;
            borders.minY = _activeCamera.ViewportToWorldPoint(Vector2.zero).y + borders.minYOffset;
            borders.maxX = _activeCamera.ViewportToWorldPoint(Vector2.right).x - borders.maxXOffset;
            borders.maxY = _activeCamera.ViewportToWorldPoint(Vector2.up).y - borders.maxYOffset;
        }

        public void Terminate()
        {
            throw new System.NotImplementedException();
        }
    }

    [System.Serializable]
    public class Borders
    {
        public float minXOffset = 1.5f, maxXOffset = 1.5f, minYOffset = 1.5f, maxYOffset = 1.5f;
        [HideInInspector] public float minX, maxX, minY, maxY;
    }
}
