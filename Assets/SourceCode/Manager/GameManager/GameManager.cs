﻿using System.Collections.Generic;
using SF.Core;
using SF.Systems;
using SF.Util;
using UnityEngine;
namespace SF.Managers
{
    public class GameManager : SingletonBase<GameManager>, IInitialisable
    {
        private GameState CurrentState { get; set;}
        private List<IGameStateListener> gameStateListeners;

        private IInitialisable _playerSystem;
        private IInitialisable _enemySystem;
        private IInitialisable _scoreSystem;
        private IInitialisable _puSystem;


        public void Init(object parm)
        {
            gameStateListeners = new List<IGameStateListener>();

            this.WaitForTime(0, () =>
            {
                UpdateState(GameState.Splash);
            });
        }

        public void ResolveDependancies()
        {
            _playerSystem = PlayerSystem.Instance;
            _enemySystem = EnemySystem.Instance;
            _scoreSystem = ScoreSystem.Instance;
            _puSystem = PowerUpSystem.Instance;

            //Debug.Log(_puSystem.GetType());

            _playerSystem.Init(null);
            _enemySystem.Init(null);
            _scoreSystem.Init(null);
            _puSystem.Init(null);
        }


        public void SubscribeForStateChange(IGameStateListener listener)
        {
            if (!gameStateListeners.Contains(listener))
                gameStateListeners.Add(listener);
        }

        public void UnsubscribeForStateChange(IGameStateListener listener)
        {
            if (gameStateListeners.Contains(listener))
                gameStateListeners.Remove(listener);
        }

        public void UpdateState(GameState updatedState, object parms = null)
        {
            //Debug.Log("State Update : Prev " + CurrentState + " Curr " + updatedState);

            if (updatedState != CurrentState)
            {
                _BroadCastStateChange(CurrentState, updatedState, parms);
                CurrentState = updatedState;
            }
        }

        private void _BroadCastStateChange(GameState prevState, GameState curState, object parms = null)
        {

            for (int i = 0; i < gameStateListeners.Count; i++)
            {
                if (gameStateListeners[i] != null)
                    gameStateListeners[i].OnStateChange(prevState, curState, parms);
                else
                {
                    Debug.Log(i + " Is null");
                }
            }
        }

        public void Terminate()
        {
            throw new System.NotImplementedException();
        }
    }
}
