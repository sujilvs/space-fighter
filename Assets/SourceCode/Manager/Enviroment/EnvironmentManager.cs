﻿using SF.Core;
using SF.Util;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace SF.Managers
{
    public class EnvironmentManager : SingletonBase<EnvironmentManager>, IInitialisable
    {
        public GameObject[] planets;
        public float timeBetweenPlanets;
        public float planetsSpeed;
        List<GameObject> planetsList = new List<GameObject>();

        public void Init(object parm)
        {
        }

        public void ResolveDependancies()
        {
            StartCoroutine(PlanetsCreation());
        }

        public void Terminate()
        {
            throw new System.NotImplementedException();
        }

        private IEnumerator PlanetsCreation()
        {
            for (int i = 0; i < planets.Length; i++)
            {
                planetsList.Add(planets[i]);
            }
            yield return new WaitForSeconds(10);
            while (true)
            {
                ////choose random object from the list, generate and delete it
                int randomIndex = Random.Range(0, planetsList.Count);
                GameObject newPlanet = Instantiate(planetsList[randomIndex]);
                planetsList.RemoveAt(randomIndex);
                //if the list decreased to zero, reinstall it
                if (planetsList.Count == 0)
                {
                    for (int i = 0; i < planets.Length; i++)
                    {
                        planetsList.Add(planets[i]);
                    }
                }
                newPlanet.GetComponent<DirectMoving>().speed = planetsSpeed;
                yield return new WaitForSeconds(timeBetweenPlanets);
            }
        }
    }
}
