﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Messenger for passing signals between modules
/// </summary>
public class Messenger
{
    /********************************************* Variables *********************************************/
    private Dictionary<string, List<Action<ISignal>>> _signalMap;
    private static Messenger _instance;
    /******************************************* Constructors *********************************************/
    private Messenger()
    {
        _signalMap = new Dictionary<string, List<Action<ISignal>>>();
    }

    ~Messenger()
    {
        _signalMap.Clear();
        _signalMap = null;
    }
    /******************************************** Singleton ***********************************************/
    public static Messenger Get()
    {
        if (_instance == null)
        {
            _instance = new Messenger();
        }
        return _instance;
    }

    /****************************************** Private Methods *******************************************/

    /****************************************** Public Methods ********************************************/
    /// <summary>
    /// For Broadcasting signal from one module
    /// </summary>
    /// <param name="signal"></param>
    public void BroadCast(ISignal signal)
    {
        if (_signalMap.ContainsKey(signal.SignalType))
        {
            var receivers = _signalMap[signal.SignalType];
            int receiversCount = receivers.Count;
            for (int i = 0; i < receiversCount; i++)
            {
                receivers[i].Invoke(signal);
            }
        }
        else
        {
            Debug.LogError("No listner found for " + signal.SignalType);
        }
    }
    /// <summary>
    /// Adding a listner to the Messenger
    /// </summary>
    /// <param name="signalType"></param>
    /// <param name="singalAction"></param>
    public void AddListner(string signalType, Action<ISignal> singalAction)
    {
        if (!_signalMap.ContainsKey(signalType))
        {
            _signalMap.Add(signalType, new List<Action<ISignal>>());
        }
        _signalMap[signalType].Add(singalAction);
    }
    /// <summary>
    /// Removing a listner from the Messenger
    /// </summary>
    /// <param name="signalType"></param>
    /// <param name="singalAction"></param>
    public void RemoveListner(string signalType, Action<ISignal> singalAction)
    {
        if (!_signalMap.ContainsKey(signalType))
        {
            return;
        }
        _signalMap[signalType].Remove(singalAction);
    }
    /****************************************** Unity Calls & Events **************************************/
    /****************************************** Unit Tests **************************************/
    /******************************************* Constructors *********************************************/
}
