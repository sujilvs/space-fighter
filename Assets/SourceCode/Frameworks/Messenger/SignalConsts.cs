﻿public class SignalConsts 
{
    public const string LEVEL_COMPLETE =  "level_complete";
    public const string USER_CREATE =  "user_create";
    public const string TEST_EVENT = "test_event";
    public const string LEVEL_LOAD = "level_load";
    public const string LEAVE_LEVEL = "leave_level";
}
