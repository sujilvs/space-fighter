﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Demo : MonoBehaviour
{
    [ContextMenu("MessangerTest")]
    private void MessangerTest()
    {
        Messenger.Get().AddListner(SignalConsts.TEST_EVENT, RSPNE1);

        Messenger.Get().AddListner(SignalConsts.TEST_EVENT, RSPNE2);

        Messenger.Get().AddListner(SignalConsts.TEST_EVENT, RSPNE3);

        Messenger.Get().BroadCast(new SingnalTest() { SignalType = SignalConsts.TEST_EVENT });

        Messenger.Get().RemoveListner(SignalConsts.TEST_EVENT, RSPNE3);

        Messenger.Get().BroadCast(new SingnalTest() { SignalType = SignalConsts.TEST_EVENT });

    }


    private void RSPNE1(ISignal signal)
    {
        Debug.Log("Received - 1");

    }
    private void RSPNE2(ISignal signal)
    {
        Debug.Log("Received - 2");

    }
    private void RSPNE3(ISignal signal)
    {
        Debug.Log("Received - 3");

    }
}

public class SingnalTest : ISignal
{
    public string SignalType { get; set; }
}
