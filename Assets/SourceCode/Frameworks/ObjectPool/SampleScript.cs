﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolObjectClass : MonoBehaviour
{

}
public class SampleScript : MonoBehaviour
{
    [SerializeField] private GameObject prefab;
    public ObjectPool<PoolObjectClass> poolObjects;

    private void Awake()
    {
        poolObjects = new ObjectPool<PoolObjectClass>(prefab, gameObject.transform);
    }

    internal void InitializePacks()
    {
        poolObjects.ReleaseAllObjects();

        var v_pack = poolObjects.GetObject();
        v_pack.gameObject.SetActive(true);
    }
}
