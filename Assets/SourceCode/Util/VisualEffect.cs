﻿using UnityEngine;
using System.Collections;
namespace SF.Util
{
    public class VisualEffect : MonoBehaviour
    {
        public float destructionTime;

        private void OnEnable()
        {
            StartCoroutine(Destruction());
        }

        private IEnumerator Destruction()
        {
            yield return new WaitForSeconds(destructionTime);
            Destroy(gameObject);
        }
    }
}


