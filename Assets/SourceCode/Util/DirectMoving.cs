﻿using UnityEngine;
namespace SF.Util
{
    public class DirectMoving : MonoBehaviour
    {
        public float speed;

        private void Update()
        {
            transform.Translate(Vector3.up * speed * Time.deltaTime);
        }
    }
}

