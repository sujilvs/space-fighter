﻿using System;
using System.Collections;
using UnityEngine;
namespace SF.Util
{
    public static class MonoExtensions
    {
        public static void WaitForTime(this MonoBehaviour monoBehaviour, float delay, Action OnTimeOut)
        {
            monoBehaviour.StartCoroutine(_WaitForTimeOut(delay, OnTimeOut));
        }

        private static IEnumerator _WaitForTimeOut(float delay, Action OnTimeOut)
        {
            yield return new WaitForSeconds(delay);
            OnTimeOut?.Invoke();
        }

        public static void MoveToPosition(this GameObject obj, Vector3 destination, float speed, bool isGlobal = true, Action OnMoveDone = null)
        {
            if(isGlobal)
                obj.GetComponent<MonoBehaviour>().StartCoroutine(_WaitForMove_Global(obj.gameObject, destination, speed, OnMoveDone));
            else
                obj.GetComponent<MonoBehaviour>().StartCoroutine(_WaitForMove_Local(obj.gameObject, destination, speed, OnMoveDone));
        }

        private static IEnumerator _WaitForMove_Global(GameObject obj, Vector3 destination, float speed, Action OnMoveDone = null)
        {
            while (Vector3.Distance(obj.transform.position, destination) > 0.1f)
            {
                obj.transform.position = Vector3.MoveTowards(obj.transform.position, destination, speed);
                yield return new WaitForFixedUpdate();
            }

            OnMoveDone?.Invoke();
        }

        private static IEnumerator _WaitForMove_Local(GameObject obj, Vector3 destination, float speed, Action OnMoveDone = null)
        {
            while (Vector3.Distance(obj.transform.localPosition, destination) > 0.1f)
            {
                obj.transform.localPosition = Vector3.MoveTowards(obj.transform.localPosition, destination, speed);
                yield return new WaitForFixedUpdate();
            }
            OnMoveDone?.Invoke();
        }
    }
}

